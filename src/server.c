/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/31 16:46:24 by lorenuar          #+#    #+#             */
/*   Updated: 2021/05/31 17:34:54 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "minitalk.h"

static t_data	g_dat;


static void	sig_handler(int code)
{
	g_dat.sig = 0;
	if (code == SIGUSR1)
	{
		g_dat.sig = 1;
	}
	else if (code == SIGUSR2)
	{
		g_dat.sig = 2;
	}
	else
	{
		exit (code);
	}

	if (g_dat.sig > 0)
	{
		if (g_dat.sig == 1)
		{
			g_dat.tmp = g_dat.tmp | (1 << g_dat.bitcnt);
		}
		else if (g_dat.sig == 2)
		{
			g_dat.tmp = g_dat.tmp ^ (1 << g_dat.bitcnt);
		}
	}



}

void print_binary(char t)
{
	for (int i = 0; i < 8; i++)
	{
		printf("%d ", (t & (1 << i)) >>	 i);
	}
	printf("\n");
}

int main(int argc, char *argv[])
{
	g_dat.eof_received = 0;
	g_dat.sig = 0;
	g_dat.str = NULL;
	g_dat.bitcnt = 0;
	signal(SIGUSR1, sig_handler);
	signal(SIGUSR2, sig_handler);

	char t = 0b01000001;

	print_binary(t);
	t =  t | (1 << 2);
	print_binary(t);
	t =  t ^ (1 << 2);
	print_binary(t);


}
