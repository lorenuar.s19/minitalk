/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minitalk.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/31 16:47:42 by lorenuar          #+#    #+#             */
/*   Updated: 2021/05/31 17:34:04 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINITALK_H
# define MINITALK_H

# include <signal.h>
# include <stddef.h>

# include <stdlib.h>
# include <stdio.h>

# define CHAR_SIZE 7

typedef struct s_data
{
	char	*str;
	char	sig;
	char	eof_received;
	char	c;
	char	tmp;
	char	bitcnt;

}	t_data;



#endif
